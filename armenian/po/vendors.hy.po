msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:28+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
#, fuzzy
msgid "Vendor"
msgstr "Մատակարարող՝"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:17
#, fuzzy
msgid "Architectures"
msgstr "Համակառույցները՝"

#: ../../english/CD/vendors/vendors.CD.def:18
#, fuzzy
msgid "Ship International"
msgstr "Առաքում այլ երկրներ՝"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr ""

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
#, fuzzy
msgid "Vendor Home"
msgstr "Մատակարարող՝"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:71
#, fuzzy
msgid "email"
msgstr "էլ-փոստ՝"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr ""

#~ msgid "Vendor:"
#~ msgstr "Մատակարարող՝"

#~ msgid "URL for Debian Page:"
#~ msgstr "Debian-ի հասցեն՝"

#~ msgid "Country:"
#~ msgstr "Երկիր՝"

#~ msgid "Ship International:"
#~ msgstr "Առաքում այլ երկրներ՝"

#~ msgid "email:"
#~ msgstr "էլ-փոստ՝"

#~ msgid "CD Type:"
#~ msgstr "CD տիպը՝"

#~ msgid "DVD Type:"
#~ msgstr "DVD տիպը՝"

#~ msgid "Architectures:"
#~ msgstr "Համակառույցները՝"

#~ msgid "Vendor Release"
#~ msgstr "Մատակարարողի "
