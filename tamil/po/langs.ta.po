msgid ""
msgstr ""
"Project-Id-Version: டெபியன் தள தமிழாக்கம்\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-10-29 22:02+0530\n"
"Last-Translator: ஆமாச்சு <amachu@ubuntu.com>\n"
"Language-Team: டெபியன் தமிழாக்கம் <vizhuthugal@anbu.in>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "அரேபிய"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "அர்மேனிய"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "பிஃன்னிஷ்"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "குரொயேஷியம்"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "டேனிஷ்"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "டச்சு"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "ஆங்கிலம்"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "பிரெஞ்சு"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "கலீசியன்"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "ஜெர்மன்"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "இத்தாலியம்"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "ஜப்பானியம்"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "கொரியம்"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "ஸ்பானிஷ்"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "போரத்துகீசிய"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "போர்த்திகீசிய (பிரேசிலிய)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "சீனம்"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "சீனம் (சைனா)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "சீனம் (ஹாங் காங்)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "சீனம் (தாய்வான்)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "சீனம் (மரபு)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "சீனம் (எளிமை)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "ஸ்வீடிஷ்"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "போலிஷ்"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "நார்வேயம்"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "துருக்கியம்"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "இரஷ்ய"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "செக்"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "எஸ்பரன்டோ"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "ஹங்கேரிய"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "ருமானிய"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "ஸ்லோவாக்"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "கிரேக்கம்"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "காடலான்"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "இந்தோனேஷிய"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "லித்துவானிய"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "ஸ்லோவேனிய"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "பல்கேரிய"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "தமிழ்"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "அப்ரிகான்"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "அலபேனிய"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr ""

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "அமஹாரிக்"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "அஸர்பைஜனி"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "பாஸ்கயூ"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "பெலாருஸ்ய"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "பெங்காளி"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "போஸ்னிய"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "ப்ரெடான்"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "காரனிஷ்"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "எஸ்டோனியன்"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "பேரோயீஸ்"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "கேலிக் (ஸ்காடஸ்)"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "ஜார்ஜியன்"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "எபிரேயம்"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "ஹிந்தி"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "ஐஸ்லாந்திய"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "இன்டர்லிங்குவா"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "ஐரீஷ்"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "கலால்லிசுட்"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "கன்னடம்"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "குர்திஷ்"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "லத்வீய"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "மசடோனிய"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "மலாய்"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "மலையாளம்"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "மாலடீஸ்"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "மாங்க்ஸ்"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "மோரி"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "மங்கோலிய"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "நார்வேயம் Bokm&aring;l"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "நார்வேயம் நைநாரஸ்க்"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "ஆசிட்டன் (1500 பின்)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "பெர்சிய"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "செர்பிய"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "ஸ்லோவேனிய"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "டாஜிக்"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "தாய்"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "டோங்கா"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "ட்வி"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "உக்ரேனிய"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "வியட்னாமிய"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "வெல்ஷ்"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "ஸோஸா"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "இட்டிஷ்"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "ஸுலு"
