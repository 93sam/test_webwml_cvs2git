# German translation of the Debian webwml modules
# Dr. Tobias Quathamer <toddy@debian.org>, 2011, 2012, 2016, 2017.
# Holger Wansing <linux@wansing-online.de>, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2017-05-15 22:58+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Übersetzungsstatistik der Debian-Website"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Es sind %d Seiten zu übersetzen."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Es sind %d Byte zu übersetzen."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Es sind %d Zeichenketten zu übersetzen."

#: ../../stattrans.pl:239 ../../stattrans.pl:243
msgid "This translation is too out of date"
msgstr "Diese Übersetzung ist zu alt"

#: ../../stattrans.pl:241 ../../stattrans.pl:437
msgid "Wrong translation version"
msgstr "Falsche Version übersetzt"

#: ../../stattrans.pl:245
msgid "The original is newer than this translation"
msgstr "Das Original ist aktueller als diese Übersetzung"

#: ../../stattrans.pl:248 ../../stattrans.pl:437
msgid "The original no longer exists"
msgstr "Das Original existiert nicht mehr"

#: ../../stattrans.pl:413
msgid "hit count N/A"
msgstr "Keine Zugriffszahlen"

#: ../../stattrans.pl:413
msgid "hits"
msgstr "Zugriffe"

#: ../../stattrans.pl:431 ../../stattrans.pl:432
msgid "Click to fetch diffstat data"
msgstr "Klicken Sie, um diffstat-Daten zu erhalten"

#: ../../stattrans.pl:446 ../../stattrans.pl:590 ../../stattrans.pl:591
msgid "Unified diff"
msgstr "Einheitliche Unterschiede"

#: ../../stattrans.pl:449 ../../stattrans.pl:590 ../../stattrans.pl:591
msgid "Colored diff"
msgstr "Farbige Unterschiede"

#: ../../stattrans.pl:551 ../../stattrans.pl:693
msgid "Created with <transstatslink>"
msgstr "Erstellt mit <transstatslink>"

#: ../../stattrans.pl:556
msgid "Translation summary for"
msgstr "Zusammenfassung der Übersetzung für"

#: ../../stattrans.pl:559 ../../stattrans.pl:717 ../../stattrans.pl:763
#: ../../stattrans.pl:806
msgid "Not translated"
msgstr "Nicht übersetzt"

#: ../../stattrans.pl:559 ../../stattrans.pl:716 ../../stattrans.pl:762
msgid "Outdated"
msgstr "Veraltet"

#: ../../stattrans.pl:559
msgid "Translated"
msgstr "Übersetzt"

#: ../../stattrans.pl:559 ../../stattrans.pl:641 ../../stattrans.pl:715
#: ../../stattrans.pl:761 ../../stattrans.pl:804
msgid "Up to date"
msgstr "Aktuell"

#: ../../stattrans.pl:560 ../../stattrans.pl:561 ../../stattrans.pl:562
#: ../../stattrans.pl:563
msgid "files"
msgstr "Dateien"

#: ../../stattrans.pl:566 ../../stattrans.pl:567 ../../stattrans.pl:568
#: ../../stattrans.pl:569
msgid "bytes"
msgstr "Byte"

#: ../../stattrans.pl:576
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Beachten Sie: Die Auflistung der Seiten ist nach der Häufigkeit der Abrufe "
"sortiert. Bewegen Sie den Mauszeiger auf den Seitennamen, um die Anzahl der "
"Zugriffe zu sehen."

#: ../../stattrans.pl:582
msgid "Outdated translations"
msgstr "Veraltete Übersetzungen"

#: ../../stattrans.pl:584 ../../stattrans.pl:640
msgid "File"
msgstr "Datei"

#: ../../stattrans.pl:586 ../../stattrans.pl:592
msgid "Diff"
msgstr "Unterschiede"

#: ../../stattrans.pl:588
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:589
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:594
msgid "Log"
msgstr "Protokoll"

#: ../../stattrans.pl:595
msgid "Translation"
msgstr "Übersetzung"

#: ../../stattrans.pl:596
msgid "Maintainer"
msgstr "Betreuer"

#: ../../stattrans.pl:598
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:599
msgid "Translator"
msgstr "Übersetzer"

#: ../../stattrans.pl:600
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:607
msgid "General pages not translated"
msgstr "Allgemeine Seiten, die nicht übersetzt sind"

#: ../../stattrans.pl:608
msgid "Untranslated general pages"
msgstr "Nicht übersetzte allgemeine Seiten"

#: ../../stattrans.pl:613
msgid "News items not translated"
msgstr "Nachrichten, die nicht übersetzt sind"

#: ../../stattrans.pl:614
msgid "Untranslated news items"
msgstr "Nicht übersetzte Nachrichten"

#: ../../stattrans.pl:619
msgid "Consultant/user pages not translated"
msgstr "Berater-/Benutzerseiten, die nicht übersetzt sind"

#: ../../stattrans.pl:620
msgid "Untranslated consultant/user pages"
msgstr "Nicht übersetzte Berater-/Benutzerseiten"

#: ../../stattrans.pl:625
msgid "International pages not translated"
msgstr "Internationale Seiten, die nicht übersetzt sind"

#: ../../stattrans.pl:626
msgid "Untranslated international pages"
msgstr "Nicht übersetzte internationale Seiten"

#: ../../stattrans.pl:631
msgid "Translated pages (up-to-date)"
msgstr "Übersetzte Seiten (aktuell)"

#: ../../stattrans.pl:638 ../../stattrans.pl:788
msgid "Translated templates (PO files)"
msgstr "Übersetzte Vorlagen (PO-Dateien)"

#: ../../stattrans.pl:639 ../../stattrans.pl:791
msgid "PO Translation Statistics"
msgstr "Statistik der PO-Übersetzung"

#: ../../stattrans.pl:642 ../../stattrans.pl:805
msgid "Fuzzy"
msgstr "Ungenau"

#: ../../stattrans.pl:643
msgid "Untranslated"
msgstr "Nicht übersetzt"

#: ../../stattrans.pl:644
msgid "Total"
msgstr "Insgesamt"

#: ../../stattrans.pl:661
msgid "Total:"
msgstr "Insgesamt:"

#: ../../stattrans.pl:695
msgid "Translated web pages"
msgstr "Übersetzte Webseiten"

#: ../../stattrans.pl:698
msgid "Translation Statistics by Page Count"
msgstr "Übersetzungsstatistik nach der Seitenanzahl"

#: ../../stattrans.pl:713 ../../stattrans.pl:759 ../../stattrans.pl:803
msgid "Language"
msgstr "Sprache"

#: ../../stattrans.pl:714 ../../stattrans.pl:760
msgid "Translations"
msgstr "Übersetzungen"

#: ../../stattrans.pl:741
msgid "Translated web pages (by size)"
msgstr "Übersetzte Webseiten (nach Größe)"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Size"
msgstr "Übersetzungsstatistik nach der Seitengröße"

#~ msgid "Created with"
#~ msgstr "Erzeugt mit"

#~ msgid "Origin"
#~ msgstr "Original"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Zugriffsdaten von %s, erfasst am %s."
