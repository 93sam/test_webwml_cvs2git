#use wml::debian::translation-check translation="b46d83944f5ba26fa289ddc2abaf9e11952db7fc"

<p>Druge BTS stranice:

<ul>
  <li><a href="./">Sustav praćenja bugova, glavna stranica sadržaja.</a>
  <li><a href="Reporting">Upute za prijavljivanje bugova.</a>
  <li><a href="Access">Pristupanje izvještajima u sustavu praćenja bugova.</a>
  <li><a href="Developer">Informacije o sustavu praćenja bugova za
      razvijatelje.</a>
  <li><a href="server-control">Informacije za razvijatelje o rukovanju
      bugovima koristeći e-mail.</a>
  <li><a href="server-refcard">Kratke upute za e-mail poslužitelje.</a>
  <li><a href="server-request">Zahtijevanje bug izvještaja e-mailom.</a>
</ul>
