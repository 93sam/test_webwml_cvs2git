#use wml::debian::translation-check translation="9e60cc63d05f74f7f6941b7ff6982e5ce00069f3"

<hrline/>
<address>
Администратори на системата за следене на грешките на Debian &lt;<email "owner@bugs.debian.org">&gt;

<p>Система за следене на грешките в Debian<br/>
Copyright &copy; 1999 Darren O. Benham, 1997, 2003 nCipher Corporation Ltd,
1994-1997 Ian Jackson.</p>
</address>
